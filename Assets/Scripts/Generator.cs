﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using SimpleJSON;

public class Generator : MonoBehaviour {

	public GameObject UI;

	public bool showPopUp;
	public static bool pause = false;

	public GameObject[] players;
	public GameObject[] hearts;
	public GameObject[] YourTurn;
	public GameObject[] star1;
	public GameObject[] star2;
	public GameObject[] heartsImage;
	public GameObject[] shades;
	public int[] lives = new int[] {3,3,3,3,3,3};
	private int currentPlayer;
	private int currentPlayer2;
	public bool gameOver = false;
	public bool canMove1 = false;
	public bool canMove2 = false;
	public string word1;
	public string word2;
	public bool countdown1 = true;
	public bool countdown2 = true;
	public bool twoOnly = false;

	public GameObject[] availableObjects;
	public GameObject[] good;
	public GameObject[] bad;
	public List<GameObject> objects;
	public List<Question> questions = new List<Question>();
	public int questionNumber = -1;
	public int maxQuestion;

	public float objectsMinX = (float)-15.5;
	public float objectsMaxX = 15.5f;
	public float objectsMinY = -7.5f;
	public float objectsMaxY = 7.5f;

	public GameObject[] time;

	public GameObject[] clock;

	private AudioSource goodJob;
	private AudioSource TryAgain;
	private AudioSource[] aSources;

	public bool[] answered = new bool[] {false,false,false,false,false,false,false};

	public GameObject Image;
	public GameObject loading;
	public GameObject loadingLogo;
	public GameObject loadingStar;

	private int spin = 0;

	public string[] line;

	public string blankText = "";

	static System.Random _random = new System.Random();

	void Awake()
	{
		TextAsset dictionary = Resources.Load ("words") as TextAsset;
		string words = dictionary.text;
		string pattern = @"\n|\r|\r\n";
		Regex rgx = new Regex(pattern);
		line = rgx.Split (words);
		aSources = gameObject.GetComponents<AudioSource>();
		goodJob = aSources[0];
		TryAgain = aSources[1];
	}

	IEnumerator WaitForResponse(float timeout, WWW www)
	{
		float time = Time.time;
		while (!www.isDone)
		{
			if (Time.time - time < timeout)
			{
				continue;
			}
			else
			{
				Debug.LogWarning("XmlRpc method timed out");
				break;
			}
		}

		yield return null;
	}

	void AddQuestion()
	{
		answered[1] = false;
		answered[2] = false;
		answered[3] = false;
		answered[4] = false;
		answered[5] = false;
		answered[6] = false;
		if (GameDataCtx.isRandom == false) {
			questionNumber = questionNumber + 1;
		} else {
			questionNumber = _random.Next (0, maxQuestion+1);
		}
		GameObject[] gameObjects = GameObject.FindGameObjectsWithTag ("Tile");
		for(var i = 0 ; i < gameObjects.Length ; i ++)
		{
			Destroy(gameObjects[i]);
		}
		if (questionNumber > maxQuestion) {
			if (!gameOver) {
				Image.SetActive (false);
				for (int i = 0; i < 6; i++) {
					GameObject endButton = GameObject.Find ("Button " + (i + 1));
					endButton.GetComponent<Button> ().interactable = false;
					YourTurn [i].SetActive (false);
					star1 [i].SetActive (false);
					star2 [i].SetActive (false);
				}
				for (int i = 0; i < 12; i++) {
					time [i].SetActive (false);
					clock [i].SetActive (false);
				}
				for (var i = 0; i < gameObjects.Length; i++) {
					Destroy (gameObjects [i]);
				}
				stop ();
				gameOver = true;
			}
		} else {
			Image.GetComponent<Image> ().sprite = questions [questionNumber].Sprite;
			char[] answerLetters = questions [questionNumber].Answer.ToCharArray ();
			char[] distractorsLetters = questions [questionNumber].Distractors.ToCharArray ();
			for (int j = 0; j < 3; j++) {
				for (int i = 0; i < answerLetters.Length; i++) {
					AddSpecificObject (answerLetters [i]);
				}
				for (int i = 0; i < distractorsLetters.Length; i++) {
					AddSpecificObject (distractorsLetters [i]);
				}
			}
			for (int j = 0; j < 3; j++) {
				for (int i = 0; i < answerLetters.Length; i++) {
					AddSpecificObject2 (answerLetters [i]);
				}
				for (int i = 0; i < distractorsLetters.Length; i++) {
					AddSpecificObject2 (distractorsLetters [i]);
				}
			}
			Image.SetActive (true);
		}
		GameDataCtx.ready = true;
		loading.SetActive (false);
		loadingLogo.SetActive (false);
		loadingStar.SetActive (false);
	}
		
		
	void Start () {
		string url1 = GameConstant.Host+"background";
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Access-Control-Allow-Credentials", "true");
		headers.Add("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
		headers.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
		headers.Add("Access-Control-Allow-Origin", "*");
		WWW www1 = new WWW (url1, null, headers);
		StartCoroutine (WaitForResponse (1F, www1));
		if (www1.isDone) {
			var data = JSON.Parse (www1.text);
			GameObject image = GameObject.Find ("Background");
			if (data ["result"] [0] ["game"].Value != "") {
				string path = data ["result"] [0] ["game"].Value;
				string url2 = GameConstant.Host + "bg/" + path;
				WWW www2 = new WWW (url2, null, headers);
				StartCoroutine (WaitForResponse (1F, www2));
				float sizeX = www2.texture.width;
				float sizeY = www2.texture.height;
				Sprite background = Sprite.Create (www2.texture, new Rect (0, 0, sizeX, sizeY), new Vector2 (0.5f, 0.5f));
				image.GetComponent<Image> ().sprite = background;
			}
		}
		if (GameDataCtx.game) {
			for (int i = 0; i < 6; i++) {
				time [i].SetActive (false);
				clock [i].SetActive (false);
			}
			for (int i = 0; i < 20; i++) {
				AddObject ();
			}
			InvokeRepeating ("AddObject", 1F, 1F);
		} else if(GameDataCtx.fun){
			currentPlayer = 0;
			currentPlayer2 = 3;
			for (int i = 0; i < 200; i++) {
				AddObject ();
			}
			for (int i = 1; i < 6; i++) {
				time [i].SetActive (false);
				clock [i].SetActive (false);
				GameObject button = GameObject.Find ("Button "+(i+1));
				button.GetComponent<Button> ().interactable = false;
			}
			for (int i = 0; i < 6; i++) {
				heartsImage [i].SetActive (true);
			}
			GameObject tile2 = (GameObject)Instantiate (availableObjects [1]);
			GameObject next = GameObject.Find ("Cellrow 1").gameObject;
			GameObject nextCellrow = next.transform.Find ("CellRow").gameObject;
			Transform nextCell = nextCellrow.transform.Find ("Cell 1").gameObject.transform;
			tile2.transform.SetParent (nextCell);
			tile2.transform.localPosition = new Vector3 (0, 0, 0); 
			tile2.transform.localScale = new Vector3 (1f, 1f, 1f);
			tile2.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Letters/" + GetLetter());
			objects.Add (tile2);
			GameObject tile = (GameObject)Instantiate (availableObjects [1]);
			GameObject next1 = GameObject.Find ("Cellrow 4").gameObject;
			GameObject nextCellrow1 = next1.transform.Find ("CellRow").gameObject;
			Transform nextCell1 = nextCellrow1.transform.Find ("Cell 1").gameObject.transform;
			tile.transform.SetParent (nextCell1);
			tile.transform.localPosition = new Vector3 (0, 0, 0); 
			tile.transform.localScale = new Vector3 (1f, 1f, 1f);
			tile.transform.rotation = Quaternion.Euler(0, 0, 180);
			tile.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Letters/" + GetLetter());
			objects.Add (tile);
			YourTurn [0].SetActive (true);
			star1 [0].SetActive (true);
			star2 [0].SetActive (true);
			YourTurn [3].SetActive (true);
			star1 [3].SetActive (true);
			star2 [3].SetActive (true);
			time [9].SetActive (true);
			clock [9].SetActive (true);
			GameObject startButton = GameObject.Find ("Button 4");
			startButton.GetComponent<Button> ().interactable = true;
			GameDataCtx.ready = true;
		}
		else {
			questionNumber = -1;
			loading.SetActive (true);
			loadingLogo.SetActive (true);
			loadingStar.SetActive (true);
			string url = GameConstant.Host+"questions";
			WWW www = new WWW (url, null, headers);
			StartCoroutine (WaitForResponse (1F, www));
			if (www.isDone) {
				var data = JSON.Parse (www.text);
				maxQuestion = data ["result"].Count - 1;
				for (int i = 0; i < data ["result"].Count; i++) {
					Question newQuest = gameObject.AddComponent<Question> ();
					newQuest.Ques = data ["result"] [i] ["question"].Value;
					newQuest.Answer = data ["result"] [i] ["answer"].Value;
					newQuest.Image = data ["result"] [i] ["img_path"].Value;
					newQuest.Distractors = data ["result"] [i] ["distractors"].Value;
					string url2 = GameConstant.Host+"images/" + newQuest.Image;
					WWW www2 = new WWW (url2, null, headers);
					StartCoroutine (WaitForResponse (1F, www2));
					float sizeX = www2.texture.width;
					float sizeY = www2.texture.height;
					newQuest.Sprite = Sprite.Create (www2.texture, new Rect (0, 0, sizeX, sizeY), new Vector2 (0.5f, 0.5f));
					questions.Add (newQuest);
				}
				AddQuestion ();
			}
		}
	}

	void Update()
	{
		for (int i = 0; i < 6; i++) {
			if (lives [i] == 0&&gameOver==false) {
				shades [i].SetActive (true);
			}
		}
		if (GameDataCtx.fun == true) {
			for (int i = 0; i < 6; i++) {
				hearts [i].GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Numbers/number_" + lives[i]);
			}
		}
		GameObject[] gameObjects = GameObject.FindGameObjectsWithTag ("Tile");
		if (GameDataCtx.game && gameObjects.Length>200) {
			CancelInvoke ("AddObject");
		}
		else if(GameDataCtx.game && !IsInvoking("AddObject") && gameObjects.Length<200){
			InvokeRepeating ("AddObject", 1F, 1F);
		}
		spin -= 1;
		loadingStar.transform.rotation = Quaternion.Euler(0, 0, spin);
		if (GameDataCtx.ready && countdown1==true) {
			GameConstant.timeRemaining -= Time.deltaTime;
		}
		if (GameDataCtx.ready && GameDataCtx.fun && countdown2==true) {
			GameConstant.timeRemaining2 -= Time.deltaTime;
		}
		Image.transform.rotation = Quaternion.Euler (0f, 0f, (float)GameConstant.timeRemaining * GameConstant.secondsToDegrees);
		if (answered[1] && answered[2] && answered[3] && answered[4] && answered[5] && answered[6]) {
			AddQuestion ();
			GameConstant.timeRemaining = GameConstant.totalTime;
		} else if (GameConstant.timeRemaining<=0F&&GameDataCtx.fun==false) {
			AddQuestion ();
			GameConstant.timeRemaining = GameConstant.totalTime;
		}
		if(GameConstant.timeRemaining<=0F&&GameDataCtx.fun==true&&gameOver==false) {
				GameObject cellrow = GameObject.Find ("Cellrow " + (currentPlayer + 1));
				submit (cellrow);
		}
		if(GameConstant.timeRemaining2<=0F&&GameDataCtx.fun==true&&gameOver==false) {
			GameObject cellrow = GameObject.Find ("Cellrow " + (currentPlayer2 + 1));
			submit (cellrow);
		}
		int count2 = 0;
		for (int i = 0; i < 6; i++) {
			if (lives [i] == 0) {
				count2 = count2 + 1;
			}
		}
		if (gameObjects.Length == 0 || count2 >= 5) {
			if (gameOver == false) {
				for (int i = 0; i < 6; i++) {
					GameObject endButton = GameObject.Find ("Button " + (i + 1));
					endButton.GetComponent<Button> ().interactable = false;
					YourTurn [i].SetActive (false);
					star1 [i].SetActive (false);
					star2 [i].SetActive (false);
				}
				for (int i = 0; i < 12; i++) {
					time [i].SetActive (false);
					clock [i].SetActive (false);
				}
				for(var i = 0 ; i < gameObjects.Length ; i ++)
				{
					Destroy(gameObjects[i]);
				}
				stop ();
				gameOver = true;
			}
		}
		int seconds = (int) GameConstant.timeRemaining;
		int minutes = 0;
		while (seconds >= 60) {
			seconds = seconds - 60;
			minutes = minutes + 1;
		}
		if (seconds < 10) {
			for (int i = 0; i < 6; i++) {
				time [i].GetComponent<Text> ().text = minutes + ":0" + seconds;
			}
		} else {
			for (int i = 0; i < 6; i++) {
				time [i].GetComponent<Text> ().text = minutes + ":" + seconds;
			}
		}
		int seconds2 = (int) GameConstant.timeRemaining2;
		int minutes2 = 0;
		while (seconds2 >= 60) {
			seconds2 = seconds2 - 60;
			minutes2 = minutes2 + 1;
		}
		if (seconds2 < 10) {
			for (int i = 6; i < 12; i++) {
				time [i].GetComponent<Text> ().text = minutes2 + ":0" + seconds2;
			}
		} else {
			for (int i = 6; i < 12; i++) {
				time [i].GetComponent<Text> ().text = minutes2 + ":" + seconds2;
			}
		}
		if (GameDataCtx.fun == false && !pause) {
			for (int j = 1; j < 7; j++) {
				bool hasSomething = false;
				for (int i = 1; i < 14; i++) {
					Transform row = GameObject.Find ("Cellrow " + j).transform.FindChild ("CellRow");
					GameObject cell = row.transform.Find ("Cell " + i).gameObject;
					if (cell.transform.childCount > 0 && answered [j] == false) {
						GameObject button = GameObject.Find ("Button " + j);
						button.GetComponent<Button> ().interactable = true;
						hasSomething = true;
					}
				}
				if (hasSomething == false) {
					GameObject button = GameObject.Find ("Button " + j);
					button.GetComponent<Button> ().interactable = false;
				}
			}
		}
			int sub = currentPlayer;
			if (sub < 5) {
				sub = sub + 1;
			} else {
				sub = 0;
			}
		if (count2 < 5) {
			while (lives [sub] == 0) {
				if (sub < 5) {
					sub = sub + 1;
				} else {
					sub = 0;
				}
			}
		}
			if (canMove1 == true && (sub!=currentPlayer2 || twoOnly) && !gameOver) {
				YourTurn [currentPlayer].SetActive (false);
				star1 [currentPlayer].SetActive (false);
				star2 [currentPlayer].SetActive (false);
				time [currentPlayer].SetActive (false);
				clock [currentPlayer].SetActive (false);
				if (currentPlayer < 5) {
					currentPlayer = currentPlayer + 1;
				} else {
					currentPlayer = 0;
				}
				while (lives [currentPlayer] == 0) {
					if (currentPlayer < 5) {
						currentPlayer = currentPlayer + 1;
					} else {
						currentPlayer = 0;
					}
				}
				time [currentPlayer].SetActive (true);
				clock [currentPlayer].SetActive (true);
				YourTurn [currentPlayer].SetActive (true);
				star1 [currentPlayer].SetActive (true);
				star2 [currentPlayer].SetActive (true);
				GameObject button2 = GameObject.Find ("Button " + (currentPlayer + 1));
				button2.GetComponent<Button> ().interactable = true;
				GameObject tile2 = (GameObject)Instantiate (availableObjects [1]);
				GameObject next = GameObject.Find ("Cellrow " + (currentPlayer + 1)).gameObject;
				GameObject nextCellrow = next.transform.Find ("CellRow").gameObject;
				Transform nextCell = nextCellrow.transform.Find ("Cell 1").gameObject.transform;
				tile2.transform.SetParent (nextCell);
				tile2.transform.localPosition = new Vector3 (0, 0, 0); 
				tile2.transform.localScale = new Vector3 (1f, 1f, 1f);
				if (next.transform.name.Equals ("Cellrow 1") || next.transform.name.Equals ("Cellrow 2")) {
					tile2.transform.rotation = Quaternion.Euler (0, 0, 0);
				} else if (next.transform.name.Equals ("Cellrow 3")) {
					tile2.transform.rotation = Quaternion.Euler (0, 0, 90);
				} else if (next.transform.name.Equals ("Cellrow 4") || next.transform.name.Equals ("Cellrow 5")) {
					tile2.transform.rotation = Quaternion.Euler (0, 0, 180);
				} else if (next.transform.name.Equals ("Cellrow 6")) {
					tile2.transform.rotation = Quaternion.Euler (0, 0, 270);
				}
				tile2.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Letters/" + word1);
				objects.Add (tile2);
				canMove1 = false;
				countdown1 = true;
			}
			int sub2 = currentPlayer2;
			if (sub2 < 5) {
				sub2 = sub2 + 1;
			} else {
				sub2 = 0;
			}
		if (count2 < 5) {
			while (lives [sub2] == 0) {
				if (sub2 < 5) {
					sub2 = sub2 + 1;
				} else {
					sub2 = 0;
				}
			}
		}
			if (canMove2 == true && (sub2!=currentPlayer || twoOnly) && !gameOver) {
				YourTurn [currentPlayer2].SetActive (false);
				star1 [currentPlayer2].SetActive (false);
				star2 [currentPlayer2].SetActive (false);
				time [currentPlayer2+6].SetActive (false);
				clock [currentPlayer2+6].SetActive (false);
				GameObject button = GameObject.Find ("Button " + (currentPlayer2 + 1));
				button.GetComponent<Button> ().interactable = false;
				if (currentPlayer2 < 5) {
					currentPlayer2 = currentPlayer2 + 1;
				} else {
					currentPlayer2 = 0;
				}
				while (lives [currentPlayer2] == 0) {
					if (currentPlayer2 < 5) {
						currentPlayer2 = currentPlayer2 + 1;
					} else {
						currentPlayer2 = 0;
					}
				}
				time [currentPlayer2+6].SetActive (true);
				clock [currentPlayer2+6].SetActive (true);
				YourTurn [currentPlayer2].SetActive (true);
				star1 [currentPlayer2].SetActive (true);
				star2 [currentPlayer2].SetActive (true);
				GameObject button2 = GameObject.Find ("Button " + (currentPlayer2 + 1));
				button2.GetComponent<Button> ().interactable = true;
				GameObject tile2 = (GameObject)Instantiate (availableObjects [1]);
				GameObject next = GameObject.Find ("Cellrow " + (currentPlayer2 + 1)).gameObject;
				GameObject nextCellrow = next.transform.Find ("CellRow").gameObject;
				Transform nextCell = nextCellrow.transform.Find ("Cell 1").gameObject.transform;
				tile2.transform.SetParent (nextCell);
				tile2.transform.localPosition = new Vector3 (0, 0, 0); 
				tile2.transform.localScale = new Vector3 (1f, 1f, 1f);
				if (next.transform.name.Equals ("Cellrow 1") || next.transform.name.Equals ("Cellrow 2")) {
					tile2.transform.rotation = Quaternion.Euler (0, 0, 0);
				} else if (next.transform.name.Equals ("Cellrow 3")) {
					tile2.transform.rotation = Quaternion.Euler (0, 0, 90);
				} else if (next.transform.name.Equals ("Cellrow 4") || next.transform.name.Equals ("Cellrow 5")) {
					tile2.transform.rotation = Quaternion.Euler (0, 0, 180);
				} else if (next.transform.name.Equals ("Cellrow 6")) {
					tile2.transform.rotation = Quaternion.Euler (0, 0, 270);
				}
				tile2.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Letters/" + word2);
				objects.Add (tile2);
				canMove2 = false;
				countdown2 = true;
			}
	}

	void AddObject()
	{
		GameObject obj = (GameObject)Instantiate(availableObjects[0]);

		obj.transform.SetParent (UI.transform);
		obj.transform.localScale = new Vector3 (1f, 1f, 1f);
		float randomX = UnityEngine.Random.Range(objectsMinX, objectsMaxX);
		float randomY = UnityEngine.Random.Range(objectsMinY, objectsMaxY);
		obj.transform.position = new Vector3(randomX,randomY,0); 
		float randomRotation = UnityEngine.Random.Range(0f,360f);
		obj.transform.Rotate(0,0,randomRotation);
		obj.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite> ("Letters/" + GetLetter());
		objects.Add (obj);
	}

	void AddSpecificObject(char letter)
	{
		GameObject obj = (GameObject)Instantiate(availableObjects[0]);
		obj.transform.SetParent (UI.transform);
		obj.transform.localScale = new Vector3 (1f, 1f, 1f);
		float randomX=0;
		float randomY=0;
		while(randomX > -6 && randomX < 6 && randomY > -6.5 && randomY < 6.5){
			randomX = UnityEngine.Random.Range(objectsMinX, -6);
			randomY = UnityEngine.Random.Range(objectsMinY, objectsMaxY);
		}
		obj.transform.position = new Vector3(randomX,randomY,0); 
		float randomRotation = UnityEngine.Random.Range(0f,360f);
		obj.transform.Rotate(0,0,randomRotation);
		obj.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite> ("Letters/" + letter);
		objects.Add (obj);
	}

	void AddSpecificObject2(char letter)
	{
		GameObject obj = (GameObject)Instantiate(availableObjects[0]);
		obj.transform.SetParent (UI.transform);
		obj.transform.localScale = new Vector3 (1f, 1f, 1f);
		float randomX=0;
		float randomY=0;
		while(randomX > -6 && randomX < 6 && randomY > -6.5 && randomY < 6.5){
			randomX = UnityEngine.Random.Range(6, objectsMaxX);
			randomY = UnityEngine.Random.Range(objectsMinY, objectsMaxY);
		}
		obj.transform.position = new Vector3(randomX,randomY,0); 
		float randomRotation = UnityEngine.Random.Range(0f,360f);
		obj.transform.Rotate(0,0,randomRotation);
		obj.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite> ("Letters/" + letter);
		objects.Add (obj);
	}

	void AddGood(GameObject parent)
	{
		int rand = _random.Next(0, good.Length);
		GameObject obj = (GameObject)Instantiate(good[rand]);
		obj.transform.parent = parent.transform; 
		obj.transform.localPosition = new Vector3 (0, 0, 0);
		obj.transform.localRotation = Quaternion.identity;
		goodJob.Play ();
	}

	void AddBad(GameObject parent)
	{
		int rand = _random.Next(0, bad.Length);
		GameObject obj = (GameObject)Instantiate(bad[rand]);
		obj.transform.parent = parent.transform; 
		obj.transform.localPosition = new Vector3 (0, 0, 0);
		obj.transform.localRotation = Quaternion.identity;
		TryAgain.Play ();
	}

	public static char GetLetter()
	{
		int num = _random.Next(0, 26); // Zero to 25
		char let = (char)('a' + num);
		return let;
	}

	public void submit(GameObject obj){
		if (GameDataCtx.game) {
			int count = 0;
			string word = "";
			Boolean hasWords = false;
			for (int i = 1; i < 14; i++) {
				GameObject cellrow = obj.transform.Find ("CellRow").gameObject;
				GameObject cell = cellrow.transform.Find ("Cell " + i).gameObject;
				if (cell.transform.childCount > 0) {
					GameObject tile = cell.transform.Find ("Tile(Clone)").gameObject;
					string tileName = tile.GetComponent<SpriteRenderer> ().sprite.name;
					word = word + tileName;
					Destroy (tile);
					hasWords = true;
				} 
			}
			if (!hasWords) {
				return;
			}

			for (int i = 0; i < line.Length; i++) {
				if (word == line [i]) {
					count = count + 1;
				}
			}
			if (count > 0 && word.Length > 2) {
				if (obj.transform.name.Equals ("Cellrow 1")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 1");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 2")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 2");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 3")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 3");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 4")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 4");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 5")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 5");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 6")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 6");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
			} 
			else {
				if (obj.transform.name.Equals ("Cellrow 1")) {
					GameObject parent = GameObject.Find ("Effects 1");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 2")) {
					GameObject parent = GameObject.Find ("Effects 2");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 3")) {
					GameObject parent = GameObject.Find ("Effects 3");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 4")) {
					GameObject parent = GameObject.Find ("Effects 4");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 5")) {
					GameObject parent = GameObject.Find ("Effects 5");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 6")) {
					GameObject parent = GameObject.Find ("Effects 6");
					AddBad (parent);
				}
			}
		} else if (GameDataCtx.fun == true) {
			int count2 = 0;
			for (int i = 0; i < 6; i++) {
				if (lives [i] > 0) {
					count2 = count2 + 1;
				}
			}
			if (count2 == 2) {
				twoOnly = true;
			}
			int count = 0;
			string word = "";
			for (int i = 1; i < 14; i++) {
				GameObject cellrow = obj.transform.Find ("CellRow").gameObject;
				GameObject cell = cellrow.transform.Find ("Cell " + i).gameObject;
				if (cell.transform.childCount > 0) {
					GameObject tile = cell.transform.GetChild (0).gameObject;
					string tileName = tile.GetComponent<SpriteRenderer> ().sprite.name;
					word = word + tileName;
					Destroy (tile);
				} 
			}

			for (int i = 0; i < line.Length; i++) {
				if (word == line [i]) {
					count = count + 1;
				}
			}
			if (count > 0 && word.Length > 2) {
				if (obj.transform.name.Equals ("Cellrow 1")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 1");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 2")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 2");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 3")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 3");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 4")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 4");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 5")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 5");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
				if (obj.transform.name.Equals ("Cellrow 6")) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects [0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects [1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects [2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects [3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 6");
					AddGood (parent);
					StartCoroutine (ScrollScore (scoreObjects, scoreCount, word.Length * 100));
				}
			} else {
				if (obj.transform.name.Equals ("Cellrow 1")) {
					lives [0] = lives [0] - 1;
					GameObject parent = GameObject.Find ("Effects 1");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 2")) {
					lives [1] = lives [1] - 1;
					GameObject parent = GameObject.Find ("Effects 2");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 3")) {
					lives [2] = lives [2] - 1;
					GameObject parent = GameObject.Find ("Effects 3");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 4")) {
					lives [3] = lives [3] - 1;
					GameObject parent = GameObject.Find ("Effects 4");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 5")) {
					lives [4] = lives [4] - 1;
					GameObject parent = GameObject.Find ("Effects 5");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 6")) {
					lives [5] = lives [5] - 1;
					GameObject parent = GameObject.Find ("Effects 6");
					AddBad (parent);
				}
			}
				if(obj.transform.name.Equals("Cellrow "+(currentPlayer+1))){
					word1 = word.Substring (word.Length - 1);
					canMove1 = true;
					GameObject button = GameObject.Find ("Button " + (currentPlayer + 1));
					button.GetComponent<Button> ().interactable = false;
					GameConstant.timeRemaining = GameConstant.totalTime;
					countdown1 = false;
				}
				else if(obj.transform.name.Equals("Cellrow "+(currentPlayer2+1))){
					word2 = word.Substring (word.Length - 1);
					canMove2 = true;
					GameObject button = GameObject.Find ("Button " + (currentPlayer2 + 1));
					button.GetComponent<Button> ().interactable = false;
					GameConstant.timeRemaining2 = GameConstant.totalTime;
					countdown2 = false;
				}
			}
				else {
			string word = "";
			Boolean hasWords = false;
			Boolean correct = false;
			for (int i = 1; i < 14; i++) {
				GameObject cellrow = obj.transform.Find ("CellRow").gameObject;
				GameObject cell = cellrow.transform.Find ("Cell " + i).gameObject;
				if (cell.transform.childCount > 0) {
					GameObject tile = cell.transform.Find ("Tile(Clone)").gameObject;
					string tileName = tile.GetComponent<SpriteRenderer> ().sprite.name;
					word = word + tileName;
					tile.transform.SetParent(null);
					float randomX=0;
					float randomY=0;
					while(randomX > -6 && randomX < 6 && randomY > -6.5 && randomY < 6.5){
						if (obj.transform.name.Equals ("Cellrow 1") || obj.transform.name.Equals ("Cellrow 5") || obj.transform.name.Equals ("Cellrow 6")) {
							randomX = UnityEngine.Random.Range (objectsMinX, -6);
							randomY = UnityEngine.Random.Range (objectsMinY, objectsMaxY);
						} else {
							randomX = UnityEngine.Random.Range (6, objectsMaxX);
							randomY = UnityEngine.Random.Range (objectsMinY, objectsMaxY);
						}
					}
					tile.transform.position = new Vector3(randomX,randomY,0); 
					float randomRotation = UnityEngine.Random.Range(0f,360f);
					tile.transform.Rotate(0,0,randomRotation);
					hasWords = true;
				}
			}
			if (!hasWords) {
				return;
			}
			if (word == questions [questionNumber].Answer) {
				correct = true;
			}
			if (correct) {
				if (obj.transform.name.Equals ("Cellrow 1") && answered[1] == false) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects[0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects[1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects[2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects[3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 1");
					AddGood (parent);
					StartCoroutine(ScrollScore(scoreObjects, scoreCount, 100));
					answered[1] = true;
				}
				if (obj.transform.name.Equals ("Cellrow 2") && answered[2] == false) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects[0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects[1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects[2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects[3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 2");
					AddGood (parent);
					StartCoroutine(ScrollScore(scoreObjects, scoreCount, 100));
					answered[2] = true;
				}
				if (obj.transform.name.Equals ("Cellrow 3") && answered[3] == false) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects[0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects[1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects[2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects[3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 3");
					AddGood (parent);
					StartCoroutine(ScrollScore(scoreObjects, scoreCount, 100));
					answered[3] = true;
				}
				if (obj.transform.name.Equals ("Cellrow 4") && answered[4] == false) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects[0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects[1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects[2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects[3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 4");
					AddGood (parent);
					StartCoroutine(ScrollScore(scoreObjects, scoreCount, 100));
					answered[4] = true;
				}
				if (obj.transform.name.Equals ("Cellrow 5") && answered[5] == false) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects[0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects[1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects[2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects[3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 5");
					AddGood (parent);
					StartCoroutine(ScrollScore(scoreObjects, scoreCount, 100));
					answered[5] = true;
				}
				if (obj.transform.name.Equals ("Cellrow 6") && answered[6] == false) {
					GameObject[] scoreObjects = new GameObject[4];
					scoreObjects[0] = obj.transform.Find ("score 1").gameObject;
					scoreObjects[1] = obj.transform.Find ("score 2").gameObject;
					scoreObjects[2] = obj.transform.Find ("score 3").gameObject;
					scoreObjects[3] = obj.transform.Find ("score 4").gameObject;
					string totalScore = "";
					for (int i = 0; i < 4; i++) {
						if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
							string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
							string[] number = scoreNumber.Split ('_');
							totalScore = totalScore + number [1];
						}
					}
					int scoreCount = Int32.Parse (totalScore);
					GameObject parent = GameObject.Find ("Effects 6");
					AddGood (parent);
					StartCoroutine(ScrollScore(scoreObjects, scoreCount, 100));
					answered[6] = true;
				}
			}
			else {
				if (obj.transform.name.Equals ("Cellrow 1")) {
					GameObject parent = GameObject.Find ("Effects 1");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 2")) {
					GameObject parent = GameObject.Find ("Effects 2");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 3")) {
					GameObject parent = GameObject.Find ("Effects 3");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 4")) {
					GameObject parent = GameObject.Find ("Effects 4");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 5")) {
					GameObject parent = GameObject.Find ("Effects 5");
					AddBad (parent);
				}
				if (obj.transform.name.Equals ("Cellrow 6")) {
					GameObject parent = GameObject.Find ("Effects 6");
					AddBad (parent);
				}
			}
		}
	}

	public IEnumerator ScrollScore(GameObject[] scoreObjects, int scoreCount, int scoreAdd){
		float curScore = scoreCount;
		for (int i = 0; i < scoreAdd; i++) {
			curScore = curScore + 1;
			string score = curScore.ToString ();
			for (int j = 0; j < score.Length; j++) {
				scoreObjects[j].GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Numbers/number_" + System.Convert.ToString (score [j]));
			}
			yield return new WaitForSeconds(.005f);
		}
	}

	void stop(){
		StartCoroutine (WaitOneSecond ());
	}
		
	public IEnumerator WaitOneSecond(){
		yield return new WaitForSeconds (3);
		int winner = 0;
		for (int i = 0; i < 6; i++) {
			shades [i].SetActive (true);
			if (lives [i] > 0) {
				winner = i;
			}
		}

		yield return new WaitForSeconds (3);

		int[] scoreCount = new int[6];
		for(int j=0; j<6; j++){
			GameObject obj = GameObject.Find ("Cellrow " + (j+1));
			GameObject[] scoreObjects = new GameObject[4];
			scoreObjects[0] = obj.transform.Find ("score 1").gameObject;
			scoreObjects[1] = obj.transform.Find ("score 2").gameObject;
			scoreObjects[2] = obj.transform.Find ("score 3").gameObject;
			scoreObjects[3] = obj.transform.Find ("score 4").gameObject;
			string totalScore = "";
			for (int i = 0; i < 4; i++) {
				if (scoreObjects [i].GetComponent<SpriteRenderer> ().sprite != null) {
					string scoreNumber = scoreObjects [i].GetComponent<SpriteRenderer> ().sprite.name;
					string[] number = scoreNumber.Split ('_');
					totalScore = totalScore + number [1];
				}
			}
			scoreCount[j] = Int32.Parse (totalScore);
		}
		int[] highestScore = new int[6];
		highestScore [0] = 0;
		for (int i = 1; i < 6; i++) {
			highestScore [i] = 6;
		}
		bool repeat = false;
		for (int i = 1; i < 6; i++) {
			if (scoreCount [i] > scoreCount [highestScore[0]]) {
				highestScore[0] = i;
				for (int j = 1; j < 6; j++) {
					highestScore [j] = 6;
					repeat = false;
				}
			} else if (scoreCount [i] == scoreCount [highestScore[0]]) {
				repeat = true;
				for (int j = 1; j < 6; j++) {
					if (highestScore [j] == 6) {
						highestScore [j] = i;
						break;
					}
				}
			}
		}
		if (!repeat) {
			shades[highestScore[0]].SetActive (false);
			GameObject parent = GameObject.Find ("Effects " + (highestScore [0] + 1));
			AddGood (parent);
		} else {
			for (int i = 0; i < 6; i++) {
				if (highestScore [i] != 6) {
					shades[highestScore[i]].SetActive (false);
					GameObject parent = GameObject.Find ("Effects " + (highestScore [i] + 1));
					AddGood (parent);
				}
			}
		}
		yield return new WaitForSeconds (4);
		GameConstant.timeRemaining = GameConstant.totalTime;
		GameConstant.timeRemaining2 = GameConstant.totalTime;
		GameDataCtx.ready = false;
		Application.LoadLevel("Choose");
	}
	public void goHome (){
		showPopUp = true;
		countdown1 = false;
		countdown2 = false;
		pause = true;
		CancelInvoke ("AddObject");
		if (!GameDataCtx.fun) {
			for (int i = 1; i < 7; i++) {
				GameObject button = GameObject.Find ("Button " + i);
				button.GetComponent<Button> ().interactable = false;
			}
		} else{
			GameObject button = GameObject.Find ("Button " + (currentPlayer + 1));
			button.GetComponent<Button> ().interactable = false;
			GameObject button2 = GameObject.Find ("Button " + (currentPlayer2 + 1));
			button2.GetComponent<Button> ().interactable = false;
		}
	}

	void OnGUI()
	{
		if (showPopUp)
		{
			GUI.Window(0, new Rect((Screen.width/2)-150, (Screen.height/2)-100
				, 300, 150), ShowGUI, "<b>Go to Main Menu</b>");
		}
	}

	void ShowGUI(int windowID)
	{
		var centeredStyle = GUI.skin.GetStyle("Label");
		centeredStyle.alignment = TextAnchor.UpperCenter;
		GUI.Label(new Rect(25, 40, 250, 50), "<b>Are you sure you want to quit current game and go to main menu?</b>", centeredStyle);

		if (GUI.Button (new Rect (65, 100, 75, 30), "<b>Yes</b>")) {
			showPopUp = false;
			pause = false;
			GameConstant.timeRemaining = GameConstant.totalTime;
			GameConstant.timeRemaining2 = GameConstant.totalTime;
			GameDataCtx.ready = false;
			Application.LoadLevel ("Choose");
		} else if (GUI.Button (new Rect (165, 100, 75, 30), "<b>No</b>")) {
			showPopUp = false;
			countdown1 = true;
			countdown2 = true;
			pause = false;
			if (GameDataCtx.fun) {
				GameObject button = GameObject.Find ("Button " + (currentPlayer + 1));
				button.GetComponent<Button> ().interactable = true;
				GameObject button2 = GameObject.Find ("Button " + (currentPlayer2 + 1));
				button2.GetComponent<Button> ().interactable = true;
			} else if (GameDataCtx.game) {
				InvokeRepeating ("AddObject", 1F, 1F);
			}
		}
	}
}
