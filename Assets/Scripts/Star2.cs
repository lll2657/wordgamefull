﻿using UnityEngine;
using System.Collections;

public class Star2 : MonoBehaviour {

	private float a;
	private bool change;

	void Start() {
		GetComponent<Renderer> ().material.color = new Color(1,1,1,(float)0.5);
		a = GetComponent<Renderer>().material.color.a;
	}

	void Update () {
		if (a>=1){
			change = true;
		}
		else if(a<=0.3){
			change = false;
		}
		if (change) {
			a = a - (float)0.01;
			GetComponent<Renderer> ().material.color = new Color(1,1,1,a);
		} else{
			a = a + (float)0.01;
			GetComponent<Renderer> ().material.color = new Color(1,1,1,a);
		}
	}
}
